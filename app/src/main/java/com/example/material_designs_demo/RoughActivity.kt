package com.example.material_designs_demo

import android.annotation.SuppressLint
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_rough.*

class RoughActivity : AppCompatActivity() {

    var img = arrayOf(R.drawable.medicinebox
        ,R.drawable.pharmacymedicine
        ,R.drawable.medicinebox
        ,R.drawable.pharmacymedicine)
    var name = arrayOf("B-Complex Capsule"
        ,"Elements -LIV-s Gain"
        ,"-LIV-s Gain"
        ,"Capsule")
    var belowName = arrayOf("Universal Health Sciences"
        ,"Lorem ipsum"
        ,"ipsum"
        ,"Universal")
    var items = arrayOf("X4","X2","X6","X8")
    var cash = arrayOf("$2000","$410","$7854","$10")

    @SuppressLint("ResourceAsColor")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_rough)

        var adapter1 = My_List_Adapter(this,img,name,belowName,items,cash)
        recycler_view.layoutManager=LinearLayoutManager(this)
        recycler_view.adapter = adapter1

        txt_EN.setOnClickListener {
            it.setBackgroundColor(R.color.colorAccent)

        }
        txt_hell.setOnClickListener {
            it.setBackgroundColor(R.color.colorAccent)
        }
    }
}