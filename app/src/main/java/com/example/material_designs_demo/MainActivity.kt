package com.example.material_designs_demo

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import com.google.android.material.tabs.TabLayout
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity()  {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        /**adding tabs to the layout n text*/
        tablayout!!.addTab(tablayout!!.newTab().setText("Chat"))
        tablayout!!.addTab(tablayout!!.newTab().setText("Status"))
        tablayout!!.addTab(tablayout!!.newTab().setText("Call"))

        /**adapter for the swapping of fragment at particular postion */
        val adapter = MyAdapter(this, supportFragmentManager, tablayout!!.tabCount)
        view_pager!!.adapter = adapter

        view_pager!!.addOnPageChangeListener(TabLayout.TabLayoutOnPageChangeListener(tablayout))

        tablayout!!.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener{

            override fun onTabReselected(p0: TabLayout.Tab?) {
            }

            override fun onTabUnselected(p0: TabLayout.Tab?) {
            }

            override fun onTabSelected(p0: TabLayout.Tab) {
                view_pager.currentItem=p0.position
            }
        })
        /**bottom navigation view code below*/
        btm_navi.setOnNavigationItemSelectedListener {
            when (it.itemId) {
                R.id.nav_item_one -> {
                    setContent("Home")
                    true
                }
                R.id.nav_item_two -> {
                    setContent("Notification")
                    true
                }
                R.id.nav_item_three -> {
                    setContent("Search")
                    true
                }
                R.id.nav_item_four -> {
                    setContent("Call")
                    true
                }
                R.id.nav_item_five -> {
                    setContent("Video")
                    true
                }
                else -> false
            }
        }
    }
    private fun setContent(s: String) {
        setTitle(s)
        txt_bottom.text = s
    }
    /**toolbar as action bar code*/
    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.activity_main_drawer,menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item.itemId){
            R.id.item_action1 ->
                Toast.makeText(this,"action1",Toast.LENGTH_LONG).show()
            R.id.item_action2 ->
                Toast.makeText(this,"action2",Toast.LENGTH_LONG).show()
            R.id.item_action3 ->
                Toast.makeText(this,"action3",Toast.LENGTH_LONG).show()
        }
        return super.onOptionsItemSelected(item)
    }
}