package com.example.material_designs_demo

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.activity_rough.view.*
import kotlinx.android.synthetic.main.custom_listview.view.*
import kotlinx.android.synthetic.main.custom_listview.view.img_mrdi1
import kotlinx.android.synthetic.main.custom_listview.view.txt_2000
import kotlinx.android.synthetic.main.custom_listview.view.txt_x4
import kotlinx.android.synthetic.main.rough_design.view.*

class My_List_Adapter(
    var context : Context,
    img: Array<Int>,
    name: Array<String>,
    belowName: Array<String>,
    items: Array<String>,
    cash: Array<String>
): RecyclerView.Adapter<My_List_Adapter.ViewHolder>() {

    var image = img
    var name1 = name
    var belowName1 = belowName
    var items1 = items
    var cash1 = cash


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int):ViewHolder {

        var view = LayoutInflater.from(context).inflate(R.layout.custom_listview,parent,false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return image.size

    }

    override fun onBindViewHolder(holder: My_List_Adapter.ViewHolder, position: Int) {

        holder.itemView.img_mrdi1.setImageResource(image[position])
        holder.itemView.txt_view1.text = name1[position]
        holder.itemView.txt_view2.text = belowName1[position]
        holder.itemView.txt_x4.text = items1[position]
        holder.itemView.txt_2000.text = cash1[position]
    }

    class ViewHolder(view : View) : RecyclerView.ViewHolder(view) {
        fun srt(){

        }

    }
}