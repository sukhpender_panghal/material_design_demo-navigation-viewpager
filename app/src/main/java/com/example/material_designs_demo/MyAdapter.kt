package com.example.material_designs_demo

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter

class MyAdapter(mainActivity: MainActivity
                ,supportFragmentManager: FragmentManager
                ,var totalTabs: Int)
    : FragmentPagerAdapter(supportFragmentManager) {

    override fun getItem(position: Int): Fragment {
        when (position) {
            0 -> {
                return Chat_Fragment()
            }
            1 -> {
                return StatusFragment()
            }
            2 -> {
                return CallsFragment()
            }
            else -> return null!!
        }
    }

    override fun getCount(): Int {

        return totalTabs
    }
}
